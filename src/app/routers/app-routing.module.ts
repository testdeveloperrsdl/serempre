import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginGuard } from '../guards/login.guard';
import { ErrorComponent } from '../pages/error/error.component';
import { HomeComponent } from '../pages/home/home.component';
import { ListProductsComponent } from '../pages/list-products/list-products.component';

import { LoginComponent } from '../pages/login/login.component';
import { ProductComponent } from '../pages/product/product.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  // Ruta principal
  {
    path: 'home', component: HomeComponent,
    canActivate: [LoginGuard],
    children: [
      { path: '', redirectTo: '/home/list-products', pathMatch: 'full' },
      { path: 'list-products', component: ListProductsComponent },
      { path: 'product/:id', component: ProductComponent },
    ]
  },
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: '**', component: ErrorComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
