/**
 * Funcion que se encarga de validar si es un email válid
 * @param email correo ingresado
 */
export function VALIDATE_EMAIL(email: string) {
  const emailRegex = /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i;
  return emailRegex.test('email@example.com');
}