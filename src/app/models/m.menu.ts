import { IProductsOptions } from '../interfaces/i.products.options'
/**
 * 
 */
export const MENU_PRODUCT = [
  'Overview',
  'Features',
  'What´s in'
]
/**
 * Mocks
 */
export const FEATURE_OPTIONS = [
  {
    title: 'Custom touch control',
    icon: 1
  },
  {
    title: 'Active nose cancellation',
    icon: 2
  },
  {
    title: 'Build in equalizer',
    icon: 3
  }
]
/**
 * Mocks
 */
export const MENU_CHOOSE: IProductsOptions[] = [
  {
    title: 'Ivory White',
    description: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Minima vitae, obcaecati consectetur dignissimos nam nisi quam temporibus dolore error tempore aspernatur quia officiis, maiores magnam veniam quisquam dolor qui sint.'
  },
  {
    title: 'Matte black',
    description: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Minima vitae, obcaecati consectetur dignissimos nam nisi quam temporibus dolore error tempore aspernatur quia officiis, maiores magnam veniam quisquam dolor qui sint.'
  },
]
/**
 * Mocks
 */
export const MENU_EXTENDED: IProductsOptions[] = [
  {
    title: '2 years coverage',
    description: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Minima vitae, obcaecati consectetur dignissimos nam nisi quam temporibus dolore error tempore aspernatur quia officiis, maiores magnam veniam quisquam dolor qui sint.',
    price: 0
  },
  {
    title: '3 yearss coverage',
    description: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Minima vitae, obcaecati consectetur dignissimos nam nisi quam temporibus dolore error tempore aspernatur quia officiis, maiores magnam veniam quisquam dolor qui sint.',
    price: 75
  },
]
/**
 * Mocks
 */
export const MENU_FEATURE: IProductsOptions[] = [
  {
    title: 'Voice Assisten support',
    price: 0
  },
  {
    title: 'Customizable controls',
    price: 25
  },
]
/**
 * Mocks
 */
export const MENU_SPECIFICATION: IProductsOptions[] = [
  {
    title: 'Dimensions',
    description: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit.'
  },
  {
    title: 'USB Standard',
    description: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit.'
  },
  {
    title: 'Frecuency response',
    description: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit.'
  },
  {
    title: 'Noise cancelation',
    description: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit.'
  },
]