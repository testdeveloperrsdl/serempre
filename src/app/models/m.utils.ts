import { IUser } from '../interfaces/i.user';

/**
 * Funcion que se encargar de convertir un usuarioFirebase en user
 * @param fireUser 
 */
export function PARSE_USER_FIREBASE(fireUser: firebase.User): IUser {
  const user: IUser = {
    name: fireUser.displayName,
    email: fireUser.email,
    uid: fireUser.uid,
    token: fireUser.refreshToken,
    photo: fireUser.photoURL
  };
  return user;
}