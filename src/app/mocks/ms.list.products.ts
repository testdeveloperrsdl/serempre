import { IProducts } from '../interfaces/i.products';

export const LIST_PRODUCTS_MOCK: IProducts[] = [
  {
    title: 'Audifonos iPhone 5',
    description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae delectus, quas, nulla similique accusantium voluptatibus eius impedit, eligendi in repellendus a libero cumque necessitatibus saepe nihil aliquam omnis iste magnam.',
    create: new Date,
    uid: '1',
    image: 'https://http2.mlstatic.com/audifonos-auriculares-manos-libres-earpods-para-iphone-5-6--D_NQ_NP_935699-MCO41833069682_052020-F.webp',
    images: [
      "https://cdn.shopify.com/s/files/1/0055/6400/6434/products/combo-xiaomi-mi-smart-band-4-redmi-airdots-audifonos-2_1000x1000.progressive.jpg?v=1578090564",
      "https://http2.mlstatic.com/audifonos-bluetooth-42-otium-inalambricos-resistente-agua-D_NQ_NP_755588-MCO31111825510_062019-F.jpg",
      "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQ3Z45w1ovA0yxSwd2hf93Dou6mkBX8uT0_SQ&usqp=CAU",
      "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcR2AD9FaqW_f4P-0FlMkjG-W9-iJH-NmbTlLg&usqp=CAU",
      "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQ0ptEBTSkv3uPwk4uWCtsorVbR96slnp-FcQ&usqp=CAU",
    ],
    price: 13000,
    isNew: true,
    subTitle: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae delectus,',
    extended: [
      {
        title: '2 years coverage',
        description: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Minima vitae, obcaecati consectetur dignissimos nam nisi quam temporibus dolore error tempore aspernatur quia officiis, maiores magnam veniam quisquam dolor qui sint.',
        price: 0
      },
      {
        title: '3 years coverage',
        description: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Minima vitae, o',
        price: 40000
      }
    ],
    feature: [
      {
        title: 'Voice Assisten support',
        description: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Minima vitae, obcaecati consectetur dignissimos nam nisi quam temporibus dolore error tempore aspernatur quia officiis, maiores magnam veniam quisquam dolor qui sint.',
        price: 0
      },
      {
        title: 'Customizable controls',
        description: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Minima vitae, o',
        price: 25000
      }
    ],
    specification: [
      {
        title: 'Dimensions',
        description: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit.'
      },
      {
        title: 'USB Standard',
        description: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit.'
      },
      {
        title: 'Frecuency response',
        description: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit.'
      },
      {
        title: 'Noise cancelation',
        description: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit.'
      },
    ],
    choose: [
      {
        title: 'Ivory White',
        description: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit.Lorem ipsum dolor sit, amet consectetur adipisicing elit. Minima vitae, obcaecati consectetur dignissimos nam nisi quam temporibus dolore error tempore aspernatur quia officiis, maiores magnam veniam quisquam dolor qui sint.'
      },
      {
        title: 'Matte black',
        description: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit.Lorem ipsum dolor sit, amet consectetur adipisicing elit. Minima vitae,  Lorem ipsum dolor sit, amet consectetur adipisicing elit.'
      },
    ]

  },
  {
    title: 'Audifonos remote',
    description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae delectus, quas, nulla similique accusantium voluptatibus eius impedit, eligendi in repellendus a libero cumque necessitatibus saepe nihil aliquam omnis iste magnam.',
    create: new Date,
    uid: '2',
    image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQ0ptEBTSkv3uPwk4uWCtsorVbR96slnp-FcQ&usqp=CAU',
    price: 83000,
    isNew: false,
    images: [
      "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQ0ptEBTSkv3uPwk4uWCtsorVbR96slnp-FcQ&usqp=CAU",
      "https://http2.mlstatic.com/audifonos-bluetooth-42-otium-inalambricos-resistente-agua-D_NQ_NP_755588-MCO31111825510_062019-F.jpg",
      "https://media.aws.alkosto.com/media/catalog/product/cache/6/image/69ace863370f34bdf190e4e164b6e123/6/9/6934177709968_1.jpg",
      "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcR2AD9FaqW_f4P-0FlMkjG-W9-iJH-NmbTlLg&usqp=CAU",
      "https://cnet1.cbsistatic.com/img/IwVCnEkHb6YldzDhBCW3N0tJThE=/470x353/2018/11/01/c05996ad-6fbb-4c48-887c-e2a378e5c46c/sony-1000xm3.jpg",
    ],
  },
  {
    title: 'Audifonos beats',
    description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae delectus, quas, nulla similique accusantium voluptatibus eius impedit, eligendi in repellendus a libero cumque necessitatibus saepe nihil aliquam omnis iste magnam.',
    create: new Date,
    uid: '3',
    image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSv0LtC6Ixd4lhb4J5IQaA9QxQewjrzKoJHOUtLYHREQ-FCqq8MZMZPLBC5cGYnv4zpugJO1qs&usqp=CAc',
    price: 67000
  },
  {
    title: 'White sound',
    description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae delectus, quas, nulla similique accusantium voluptatibus eius impedit, eligendi in repellendus a libero cumque necessitatibus saepe nihil aliquam omnis iste magnam.',
    create: new Date,
    uid: '',
    image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTlYxiVag2iOt0wJ6KBs2W84PJLEs9rIT74GA&usqp=CAU',
    price: 43000
  },
  {
    title: 'Audifonos diadema',
    description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae delectus, quas, nulla similique accusantium voluptatibus eius impedit, eligendi in repellendus a libero cumque necessitatibus saepe nihil aliquam omnis iste magnam.',
    create: new Date,
    uid: '',
    image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcR7uA3asv3ytmf45sdnsHUns2nucG8Xl3xTM3U_MS8SzH-_yGYH84yJzEOW4ol9rhHYljLm4Rw&usqp=CAc',
    price: 13000
  },
  {
    title: 'Audifonos iPhone 5',
    description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae delectus, quas, nulla similique accusantium voluptatibus eius impedit, eligendi in repellendus a libero cumque necessitatibus saepe nihil aliquam omnis iste magnam.',
    create: new Date,
    uid: '',
    image: 'https://http2.mlstatic.com/audifonos-auriculares-manos-libres-earpods-para-iphone-5-6--D_NQ_NP_935699-MCO41833069682_052020-F.webp',
    price: 13000
  },
  {
    title: 'Audifonos game revolution',
    description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae delectus, quas, nulla similique accusantium voluptatibus eius impedit, eligendi in repellendus a libero cumque necessitatibus saepe nihil aliquam omnis iste magnam.',
    create: new Date,
    uid: '',
    image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQ3Z45w1ovA0yxSwd2hf93Dou6mkBX8uT0_SQ&usqp=CAU',
    price: 13000
  },
  {
    title: 'AKG 8',
    description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae delectus, quas, nulla similique accusantium voluptatibus eius impedit, eligendi in repellendus a libero cumque necessitatibus saepe nihil aliquam omnis iste magnam.',
    create: new Date,
    uid: '',
    image: 'https://audioluces.com/wp-content/uploads/2017/05/audifono-profesional-akg-k52-colombia-2.jpg',
    price: 13000
  },
  {
    title: 'Celular accesorio',
    description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae delectus, quas, nulla similique accusantium voluptatibus eius impedit, eligendi in repellendus a libero cumque necessitatibus saepe nihil aliquam omnis iste magnam.',
    create: new Date,
    uid: '',
    image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSlb-Uwgdc9Wp-dR1hTXl3V5s9zRla5oqNr_g&usqp=CAU',
    price: 89000
  },
  {
    title: 'Audifonos Gamer',
    description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae delectus, quas, nulla similique accusantium voluptatibus eius impedit, eligendi in repellendus a libero cumque necessitatibus saepe nihil aliquam omnis iste magnam.',
    create: new Date,
    uid: '',
    image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcR0fCmcCsHPDt978ajbsl3qdBE4hIJbmk847w&usqp=CAU',
    price: 13000
  },
  {
    title: 'Audifonos para niña',
    description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae delectus, quas, nulla similique accusantium voluptatibus eius impedit, eligendi in repellendus a libero cumque necessitatibus saepe nihil aliquam omnis iste magnam.',
    create: new Date,
    uid: '',
    image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRD6YHmvdn1DT1JxLYL4E64PIiP_CI9mMSeXA&usqp=CAU',
    price: 43000
  },
  {
    title: 'Audifonos remote segunda generación',
    description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae delectus, quas, nulla similique accusantium voluptatibus eius impedit, eligendi in repellendus a libero cumque necessitatibus saepe nihil aliquam omnis iste magnam.',
    create: new Date,
    uid: '',
    image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQ0ptEBTSkv3uPwk4uWCtsorVbR96slnp-FcQ&usqp=CAU',
    price: 29000
  },
]