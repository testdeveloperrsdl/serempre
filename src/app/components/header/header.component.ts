import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/firebase/auth.service';
import { GlobalService } from 'src/app/services/utils/global/global.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private global: GlobalService,
    private fireAuth: AuthService,) { }

  ngOnInit(): void {
  }
  
  get currentUser(){
    return this.global.user
  }

  /**
   * Funcion que se encarga de cerrar sesion
   */
  public closeSession(){
    this.fireAuth.logOut().then(()=>{
    }).catch((err)=>{
      console.error(err);
    })
  }

}
