import { Component, Input, OnInit } from '@angular/core';
import { IProductsOptions } from 'src/app/interfaces/i.products.options';

@Component({
  selector: 'app-card-product-specification',
  templateUrl: './card-product-specification.component.html',
  styleUrls: ['./card-product-specification.component.css']
})
export class CardProductSpecificationComponent implements OnInit {
  @Input() title: string = 'Titulo';
  @Input() listSpecification: IProductsOptions[];
  constructor() { }

  ngOnInit(): void {
  }

}
