import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardProductSpecificationComponent } from './card-product-specification.component';

describe('CardProductSpecificationComponent', () => {
  let component: CardProductSpecificationComponent;
  let fixture: ComponentFixture<CardProductSpecificationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardProductSpecificationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardProductSpecificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
