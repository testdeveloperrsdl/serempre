import { IfStmt } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IProducts } from 'src/app/interfaces/i.products';
import { LIST_PRODUCTS_MOCK } from 'src/app/mocks/ms.list.products';
import { FirestoreService } from 'src/app/services/firebase/firestore.service';
import { GlobalService } from 'src/app/services/utils/global/global.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  public searchText = "";
  public timerSearchText: any;
  public listProducts: IProducts[] = [];
  public listProductsFilter: IProducts[] = [];
  
  

  constructor(
    private global: GlobalService,
    private router: Router,
    private fireStore: FirestoreService
  ) { }

  ngOnInit(): void {
    // Mocks
    // this.listProducts = LIST_PRODUCTS_MOCK;
    // this.listProductsFilter = LIST_PRODUCTS_MOCK;
    this.loadProducts();
  }

  /**
   * Funcion que se encarga de hacer el filtro por titulo sobre los productos
   * @param data texto para filtrar
   */
  public searchProduct(data: string) {
    clearTimeout(this.timerSearchText);
    if (data.length < 1) {
      this.listProductsFilter = this.listProducts;
      return;
    }
    this.timerSearchText = setTimeout(() => {
      this.listProductsFilter = this.listProducts.filter((product) => {
        return product.title.toLowerCase().includes(data.toLowerCase());
      })
    }, 800);
  }

  /**
   * Funcion que se encarga de redirigir a la vista del detalle del producto
   * @param product producto seleccionado
   */
  public showProductDetail(product: IProducts) {
    this.router.navigateByUrl(`/home/product/${product.uid}`, { queryParams: { product } });
  }

  /**
   * Funcion que se encarga de cargar los productos
   */
  public loadProducts(){
    if(this.global.listProducts && this.global.listProducts.length > 0){
      this.listProducts = this.global.listProducts;
      this.listProductsFilter = this.listProducts.filter((product)=> product.uid );
    }else{
      this.fireStore.getCollection('/products').then((data)=>{
        this.listProducts = data.docs.map(doc => doc.data() as IProducts);  
        this.listProductsFilter = this.listProducts.filter((product)=> product.uid );
        this.global.listProducts =  this.listProducts;
      }).catch((err)=>{
        console.error(err);
        
      })
    }
    
  }
}
