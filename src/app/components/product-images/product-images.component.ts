import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-product-images',
  templateUrl: './product-images.component.html',
  styleUrls: ['./product-images.component.css']
})
export class ProductImagesComponent implements OnInit {

  @Input() images: string[];
  public currentImage = '';
  public currentIndex = 0;
  constructor() { }

  ngOnInit(): void {
    this.currentImage = this.images[0];
  }

  /**
   * Funcion que se encarga de seleccionar una imagen
   * @param index Index de la imagen seleccioanda
   */
  public selectImage(index: number) {
    if (this.currentIndex == index) return;
    this.currentIndex = index;
    this.currentImage = this.images[index];
  }

}
