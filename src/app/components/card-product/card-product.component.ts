import { Component, Input, OnInit } from '@angular/core';
import { IProducts } from 'src/app/interfaces/i.products';

@Component({
  selector: 'app-card-product',
  templateUrl: './card-product.component.html',
  styleUrls: ['./card-product.component.css']
})
export class CardProductComponent implements OnInit {

  @Input() product: IProducts;

  constructor() { }

  ngOnInit(): void {
  }



}
