import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IProductsOptions } from 'src/app/interfaces/i.products.options';

@Component({
  selector: 'app-card-product-options',
  templateUrl: './card-product-options.component.html',
  styleUrls: ['./card-product-options.component.css']
})
export class CardProductOptionsComponent implements OnInit {

  @Input() title: string = 'Titulo';
  @Input() enableClick: boolean = false;
  @Input() productsOptions: IProductsOptions[];
  @Output() changePrice = new EventEmitter<IProductsOptions>();

  public currentIndex = 0;

  constructor() { }

  ngOnInit(): void {
    this.changePrice.emit(this.productsOptions[0]);
  }

  /**
   * Funcion que se encarga de seleccionar el item
   * @param i Index del item
   */
  public selectItem(i){
    if(this.enableClick){
      if(this.currentIndex==i) return;
      this.currentIndex = i;
      this.changePrice.emit(this.productsOptions[i]);
    }
  }

}
