import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardProductOptionsComponent } from './card-product-options.component';

describe('CardProductOptionsComponent', () => {
  let component: CardProductOptionsComponent;
  let fixture: ComponentFixture<CardProductOptionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardProductOptionsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardProductOptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
