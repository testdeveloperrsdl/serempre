import { GoogleButtonComponent } from './buttons/google-button/google-button.component';
import { HeaderComponent } from './header/header.component';
import { CarrouselComponent } from './carrousel/carrousel.component';
import { CardProductComponent } from './card-product/card-product.component';
import { ProductsComponent } from './products/products.component';
import { CardProductOptionsComponent } from './card-product-options/card-product-options.component';
import { CardProductSpecificationComponent } from './card-product-specification/card-product-specification.component';
import { ProductImagesComponent } from './product-images/product-images.component';
import { LoginButtonComponent } from './buttons/login-button/login-button.component';

export const COMPONENTS = [
  GoogleButtonComponent,
  HeaderComponent,
  CarrouselComponent,
  CardProductComponent,
  ProductsComponent,
  CardProductOptionsComponent,
  CardProductSpecificationComponent,
  ProductImagesComponent,
  LoginButtonComponent
];