import { Injectable } from '@angular/core';
import { IProducts } from 'src/app/interfaces/i.products';
import { IUser } from 'src/app/interfaces/i.user';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {

  private _user: IUser;
  private _listProducts: IProducts[];

  constructor() { }


  set user(user: IUser) {
    this._user = user;
  }

  get user(){
    return this._user;
  }

  set listProducts(listProducts: IProducts[]) {
    this._listProducts = listProducts;
  }

  get listProducts(){
    return this._listProducts;
  }
}
