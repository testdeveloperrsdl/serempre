import { Injectable } from '@angular/core';
import { AngularFireAuth } from "@angular/fire/auth";
import * as firebase from 'firebase/app';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private afAuth: AngularFireAuth,
  ) {

  }

  /**
   * Funcion que se encarga de validar el estado de la session
   */
  public authStatus() {
    return this.afAuth.authState;
  }

  /**
   * @description Funcion que se encarga de presetar el popup del servicio con el cual 
   * se desea realziar el login. Google, Facebook, Twitter
   * @param provider Parametro que se encarga del tipo de autenticacion a ejecutar
   */
  private login(provider: firebase.auth.GoogleAuthProvider) {
    return this.afAuth.signInWithPopup(provider)
  }

  /**
   * @description Funcion que realiza el login de google
   */
  public loginGoogle() {
    const provider = new firebase.auth.GoogleAuthProvider();
    return this.login(provider)
  }

  /**
   * @description Funcion que realiza login con email y password
   * @param email Email del usuario 
   * @param password Contraseña del usuario
   */
  public loginEmailAndPassword(email: string, password: string) {
    return this.afAuth.signInWithEmailAndPassword(email, password)
  }

  /**
   * @description Funcion que se encarga de crear usuario usando los argumentos
   * @param email Email del usuario 
   * @param password Contraseña del usuario
   */
  public registreUser(email: string, password: string) {
    return this.afAuth.createUserWithEmailAndPassword(email, password)
  }

  /**
   * @description Funcion para terminar la session
   */
  public logOut() {
    return this.afAuth.signOut()
  }

  /**
   * Funcion que se encarga de ejecutar el cambio de contraseña
   * @param email correo de la cuenta
   */
  public resetPasswordWithEmail(email: string) {
    return this.afAuth.sendPasswordResetEmail(email)
  }

  public udpateUserProfile(user: firebase.User){
    return this.afAuth.updateCurrentUser(user);
  }
}
