import { Injectable } from '@angular/core';
import { AngularFirestore } from "@angular/fire/firestore";

@Injectable({
  providedIn: 'root'
})
export class FirestoreService {

   constructor(
    private afFirestore: AngularFirestore,
  ) {
  }


  ////////////////////////////////////
  /*
  /*  COLLECTION DATABASE 
  /*
  ///////////////////////////////////*/

  /**
   * Funcion que se encarga de guardar un objeto en una coleccion
   * @param collection nombre de la coleccion
   * @param data objeto que se quiere guardar tipo json
   */
  public insertInCollection(collection: string, data: any) {
    return this.afFirestore.collection(collection).add(data)
  }

  /**
 * Funcion que se encarga de guardar un objeto en una coleccion
 * @param collection nombre de la coleccion
 * @param data objeto que se quiere guardar tipo json
 */
  public insertInCollectionWithId(collection: string, id: string, data: any) {
    return this.afFirestore.collection(collection).doc(id).set(data);
  }

  /**
   * Funcion que se encarga de cargar todos los datos de una coleccion
   * @param collection nombre de la coleccion
   */
  public getCollection(collection: string) {
    return this.afFirestore.collection(collection).get().toPromise()
  }

  /**
   * Funcion que se encarga de detectar los cambios de una coleccion en tiempo 
   * real
   * @param collection nombre de la coleccion
   */
  public getCollectionChange(collection: string) {
    return this.afFirestore.collection(collection).valueChanges()
  }

  ////////////////////////////////////
  /*
  /*  DOCUMENTS DATABASE 
  /*
  ///////////////////////////////////*/

  /**
   * @description Funcion que se encarga de obtener la informacion de un documento
   * @param pathDocument collection/id
   */
  public getDocument(pathDocument: string) {
    return this.afFirestore.doc(pathDocument).get().toPromise()
  }

  /**
   * @description Funcion que se encarga de obtener un documento en tiempo real
   * @param pathDocument collection/id
   */
  public getDocumentChange(pathDocument: string) {
    return this.afFirestore.doc(pathDocument).valueChanges().toPromise()
  }

  /**
   * @description Funcion que se encarga de actualizar un documento
   * @param pathDocument collection/id
   * @param data Nueva informacion a actualziar
   */
  public updateDocument(pathDocument: string, data: any) {
    return this.afFirestore.doc(pathDocument).update(data)
  }

  /**
   * @description Funcion que se encarga de eliminiar un documento
   * @param pathDocument collection/id
   */
  public deleteDocument(pathDocument: string) {
    return this.afFirestore.doc(pathDocument).delete()
  }

   /**
    * @description Funcion que se encarga de crear y actualziar el documento creado
    * @param collection 
    * @param data 
    */ 
  public async saveAndUpdateDocument(collection: string, data: any){
    const document = await this.insertInCollection(collection, data);
    data.uid = document.id
    return this.updateDocument(`${collection}/${document.id}`,data)
  }

  ////////////////////////////////////
  /*
  /*  DOCUMENTS DATABASE 
  /*
  ///////////////////////////////////*/

  public getReference(): AngularFirestore {
    return this.afFirestore;
  }


}
