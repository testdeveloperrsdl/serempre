import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { PARSE_USER_FIREBASE } from '../models/m.utils';
import { AuthService } from '../services/firebase/auth.service';
import { GlobalService } from '../services/utils/global/global.service';

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanActivate {

  constructor(
    private global: GlobalService,
    private fireAuth: AuthService,
    private router: Router) {

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.validateUser();
  }

  /**
   * Funcion que se encarga de validar si el usuario es autenticado
   */
  private validateUser(): Promise<boolean> {
    return new Promise((resolve) => {
      this.fireAuth.authStatus().subscribe((user) => {
        if (user) {
          this.global.user = PARSE_USER_FIREBASE(user);
          resolve(true);
        }
        else {
          this.router.navigateByUrl('/login')
          resolve(false)
        }
      }, (err) => {
        resolve(false);
      })
    })
  }
}
