export interface IUser {
    name: string;
    email: string;
    photo: string;
    uid: string;
    token: string;
}