export interface IProducts {
    isNew?: boolean;
    title: string;
    subTitle?: string;
    description: string;
    create: Date;
    uid: string;
    image: string;
    images?: string[];
    price: number;
    extended?: IExtended[];
    feature?: IFeature[];
    specification?: ISpecification[];
    choose?:IChoose[];
}

export interface IExtended {
    title: string;
    description: string;
    price: number
}

export interface IFeature{
    title: string;
    description: string;
    price: number
}

export interface ISpecification{
    title: string;
    description: string;
}

export interface IChoose{
    title: string;
    description: string
}