import { title } from 'process';

export interface IProductsOptions {
  title: string;
  description?: string;
  price?: number,
}