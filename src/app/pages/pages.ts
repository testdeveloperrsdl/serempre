import { ErrorComponent } from './error/error.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { ProductComponent } from './product/product.component';
import { ListProductsComponent } from './list-products/list-products.component';

export const PAGES = [
  ErrorComponent,
  LoginComponent,
  HomeComponent,
  ProductComponent,
  ListProductsComponent
];
