import { Component, OnInit } from '@angular/core';
import { IUser } from 'src/app/interfaces/i.user';
import { APPLICATION_LINKS } from 'src/app/models/m.links';
import { VALIDATE_EMAIL } from 'src/app/models/m.validations';
import { AuthService } from 'src/app/services/firebase/auth.service';
import { GlobalService } from 'src/app/services/utils/global/global.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { PARSE_USER_FIREBASE } from 'src/app/models/m.utils';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public imageLogin = "";
  public errorMessage = "";
  public isAccepted = false;
  public isLoading = false;
  public isRegistre = false;

  public loginForm: FormGroup;

  constructor(
    private fireAuth: AuthService,
    private global: GlobalService,
    private router: Router,
    private formBuilder: FormBuilder
  ) {
    this.imageLogin = APPLICATION_LINKS.imageLogin;
  }

  ngOnInit(): void {
    this.cleanLogin();
  }

  /**
   * Funcion que se encarga de obtener el controls del formulario
   */
  get formControls() { return this.loginForm.controls; }

  /**
   * Funcion que se encarga de limpiar el form
   */
  public cleanLogin() {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(5)]],
      name: ['']
    });
  }

  /**
   * Funcion que se encarga de trabajar la respuesta y optener el usuario logeado
   * @param data 
   */
  private managerResponse(data: firebase.auth.UserCredential) {
    const user = PARSE_USER_FIREBASE(data.user);
    if (this.isRegistre) {
      this.updateUser(data.user);
    }
    this.global.user = user;
    this.router.navigateByUrl('/home');
  }

  /**
   * Funcion que se encarga de mostra un error
   * @param message 
   */
  public showMessageError(message: string) {
    this.errorMessage = message;
    setTimeout(() => {
      this.errorMessage = '';
    }, 3000)
  }

  /**
   * Funcion que se encarga de realizar las validaciones del formulario
   */
  public requestForm() {
    if (this.loginForm.invalid) {
      this.showMessageError('Verifique los campos para ingresar');
      return;
    }

    if (!this.isAccepted) {
      this.showMessageError('Acepte los terminos y condiciones');
      return;
    }

    if (this.isRegistre) this.registreUserFirebase();
    else this.loginUserEmailPassword();
  }

  /**
   * Inicio de sesion con Google
   */
  public loginGoogle() {
    this.isLoading = true;
    this.fireAuth.loginGoogle().then((data) => {
      this.managerResponse(data);
    }).catch((err) => {
      console.error(err);
      this.showMessageError(err.message);
    }).finally(() => this.isLoading = false)
  }

  /**
   * Registra un usuario en firebase
   */
  public registreUserFirebase() {
    this.isLoading = true;
    this.fireAuth.registreUser(this.loginForm.value.email, this.loginForm.value.password).then((data) => {
      this.managerResponse(data);
    }).catch((err) => {
      console.error(err);
      this.showMessageError(err.message);
    }).finally(() => this.isLoading = false)
  }

  /**
   * Actulaizar un usuario de firebase
   * @param user 
   */
  public updateUser(user: firebase.User) {
    this.isLoading = true;
    user.updateProfile({
      displayName: `${this.loginForm.value.name}`
    }).then(()=>{
      this.global.user.name = `${this.loginForm.value.name}`;
    }).catch((err) => {
      console.error(err);
      this.showMessageError(err.message);
    }).finally(() => this.isLoading = false)
  }

  /**
   * Login con usuario y contraseña
   */
  public loginUserEmailPassword() {
    this.isLoading = true;
    this.fireAuth.loginEmailAndPassword(this.loginForm.value.email, this.loginForm.value.password).then((data) => {
      this.managerResponse(data);
    }).catch((err) => {
      console.error(err);
      this.showMessageError(err.message);
    }).finally(() => this.isLoading = false)
  }

  /**
   * Boton para cerrar la vista de cerrar
   */
  public closeRegistration() {
    this.cleanLogin();
    this.isRegistre = false;
  }

  /**
   * Funcion que se encarga de registrar un usuario
   */
  public registreUser() {
    if (!this.isRegistre) {
      this.isRegistre = true;
      this.cleanLogin();
    } else {
      this.requestForm();
    }


  }




}
