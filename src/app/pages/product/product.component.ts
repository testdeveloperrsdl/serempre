import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IProducts } from 'src/app/interfaces/i.products';
import { IProductsOptions } from 'src/app/interfaces/i.products.options';
import { FEATURE_OPTIONS, MENU_PRODUCT } from 'src/app/models/m.menu';
import { FirestoreService } from 'src/app/services/firebase/firestore.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  public product: IProducts;
  public currentOption = 0;
  public ListMenu = [];
  public listFeature = [];
  public listFeatureOptions = [];
  private selectedExtended: IProductsOptions;
  private selectedFeature: IProductsOptions;
  // public listChoose = [];
  // public listExtended = [];
  // public listSpecification = [];

  constructor(
    private route: ActivatedRoute,
    private fireStore: FirestoreService
  ) {
    this.ListMenu = MENU_PRODUCT;
    this.listFeatureOptions = FEATURE_OPTIONS;
    // MOCK
    // this.listChoose = MENU_CHOOSE;
    // this.listExtended = MENU_EXTENDED;
    // this.listFeature = MENU_FEATURE;
    // this.listSpecification = MENU_SPECIFICATION; 
  }

  ngOnInit(): void {
    const uid = this.route.snapshot.paramMap.get("id");
    this.findProductDB(uid);
    // this.searchProduct( uid)
    // this.insertMockInDB();
  }

  // private searchProduct(id: string) {
  //   this.product = LIST_PRODUCTS_MOCK.filter((product) => product.uid == id)[0];
  // }

  /**
   * Funcion que se encarga de seleccionar una opcion
   * @param index 
   */
  public selectItem(index: number) {
    if (index == this.currentOption) return;
    this.currentOption = index;
  }

  /**
   * Generar una imagen aleatoria
   */
  public getRandomImage(): string {
    const number = Math.floor(Math.random() * 10);
    return `./assets/icon/${number}.png`
  }

  /**
   * Encontrar un producto
   * @param uid 
   */
  public findProductDB(uid: string) {
    this.fireStore.getDocument(`/products/${uid}`).then((data) => {
      this.product = data.data() as IProducts;
    })
  }

  /**
   * Funcion que se encarga de insertar un producto en la base de datos, se ejecuto para mocks
   */
  public insertMockInDB() {
    for (var i = 0; i < 10; i++) {
      this.product.title = `Titulo-${i}`;
      this.product.create = new Date();
      this.fireStore.saveAndUpdateDocument('/products', this.product).then(() => console.log('list')).catch((err) => console.log(err));
    }
  }

  /**
   * Funcion que se se ejecuta cuando se selecciona una opcion de los servicio de extencion
   * @param data 
   */
  public changePriceExtended(data: IProductsOptions) {
    this.selectedExtended = data;
  }
/**
   * Funcion que se se ejecuta cuando se selecciona una opcion de los servicio de caracteristicas
   * @param data 
   */
  public changePriceFeature(data: IProductsOptions) {
    this.selectedFeature = data;
  }
  /**
   * Funcion que se encarga de retornar el precio total
   */
  public getTotalPrice(): number {
    return this.product.price + (this.selectedExtended ? this.selectedExtended.price : 0) + (this.selectedFeature?this.selectedFeature.price:0);
  }

}
