# Serempre

Repositorio dedicado a la prueba tecnica para le empresa serempre. cumpliando las condiciones dadas por correo.

## Caracteristicas

1. Proyecto desarrollado en Angular
2. Se utilizo la libreria de Bootstrap para agregar diseño a las vistas adicionales
3. Las vistas correspondientes al diseño solicitado no contienen ninguna libreria
4. Se realiza una integracion con firebase. los servicios consumidos son Auth, y Firestore

## Deploy

El proyecto se encuentra desplegado en el siguiente link https://serempre-aa146.web.app

## Ejemplos

Imagen de prueba mobile

![Imagen de desarrollo 1 ](https://polly-test-2020.s3.amazonaws.com/imagen1.png)

Imagen de prueba web

![Imagen de desarrollo 1 ](https://polly-test-2020.s3.amazonaws.com/imagen2.png)

## Instalacion

1. Cloinar el repositorio
```sh
$ git clone https://reinnersdl@bitbucket.org/testdeveloperrsdl/serempre.git
```
2. Instalar librerias
```sh
$ npm install
```
3. Correr el comando
```sh
$ ng serve
```
4. Build de la aplicación
```sh
$ ng build --prod
```


## AUTOR

Reinner Steven Daza Leiva
reinner.leiva@gmail.com